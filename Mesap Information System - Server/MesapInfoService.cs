﻿using Task = System.Threading.Tasks.Task;

using Mesap.Framework;

using MesapInformationSystem.Types;

namespace MesapInformationSystem
{
    public class MesapInfoService
    {
        private const string APP_NAME = "Mesap Information Service";

        private readonly Application app;
        private readonly UserContext userContext;
        private readonly SystemDatabase systemDB;

        private MesapInfoService()
        {
            app = new Application(new ApplicationContextBuilderOverwriteMessageServer(APP_NAME, Private.MESSAGE_SERVER));
            userContext = app.Logon(Private.MESAP_USERNAME, Private.MESAP_PASSWORD);
            systemDB = new SystemDatabase(userContext);
        }
        public static readonly MesapInfoService Instance = new();

        /// <summary>
        /// Probes the Mesap system for a list of all users registered.
        /// Online users are annotated with the databases they opened.
        /// </summary>
        /// <returns>The list of users.</returns>
        public async Task<List<User>> GetUserList()
        {
            // TODO Use caching to avoid re-generating a list that is still fresh?
            return await Task.Run(() => GenerateUserListTask.Generate(app.MessageServer, systemDB));
        }

        /// <summary>
        /// Generate list of changes across all project databases and their Mesap objects.
        /// </summary>
        /// <param name="hoursBack">Positive value for the number of hours to look back in time for changes.</param>
        /// <param name="includeValues">Whether to include time series values in the results.</param>
        /// <returns>The list of changes.</returns>
        public async Task<List<Change>> GetLatestChanges(int hoursBack, bool includeValues)
        {
            // TODO Use caching to avoid re-generating a list that is still fresh?
            return await Task.Run(() => GenerateChangeListTask.Generate(systemDB, hoursBack, includeValues));
        }

        internal void Shutdown()
        {
            app.Logoff(userContext);
            app.Dispose();
        }
    }
}
