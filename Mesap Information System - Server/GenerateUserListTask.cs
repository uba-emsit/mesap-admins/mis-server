﻿using Mesap.Framework;
using Mesap.Framework.Entities;
using Mesap.MessageServer.Model;

using MesapInformationSystem.Types;

namespace MesapInformationSystem
{
    internal class GenerateUserListTask
    {
        private const string ADMIN_USERNAME = "Admin";
        private const string OFFLINE_USER_FLAG = "offline";

        internal static List<User> Generate(MessageServer server, SystemDatabase systemDB)
        {
            List<User> users = [];

            List<UserData> connectedUsers = [];
            server.ApplicationClient.GetConnectedUsersData(out connectedUsers);

            List<DatabaseDataMonitored> connectedDatabases = [];
            server.ApplicationClient.GetConnectedDatabasesData(out connectedDatabases);

            List<ApplicationDataMonitored> connectedApplications = [];
            server.ApplicationClient.GetConnectedApplicationsData(out connectedApplications);

            foreach (UserInfo info in systemDB.UserInfos.Where(info => !info.DenyInteractiveLogon && info.Id != ADMIN_USERNAME))
            {
                User user = new()
                {
                    Name = info.Name,
                    LastSeen = info.LogonDate ?? DateTime.UnixEpoch
                };

                foreach (DatabaseDataMonitored database in connectedDatabases.Where(db => db.UserNr == info.PrimaryKey))
                {
                    user.Databases.Add(database.DatabaseId);

                    // Update user seen time point from login data if available
                    if (connectedApplications.FirstOrDefault(login => login.ApplicationContextId == database.ApplicationContextId) is { } connection)
                    {
                        DateTime lastConnected = connection.UtcConnectTimepoint.ToLocalTime();
                        if (lastConnected > user.LastSeen)
                            user.LastSeen = lastConnected;
                    }
                }

                bool isOnline = connectedUsers.Any(user => user.UserId == info.Id);
                user.LoggedIn = isOnline ? user.LastSeen.ToString() : OFFLINE_USER_FLAG;

                users.Add(user);
            }

            return users.OrderByDescending(user => user.LastSeen).ToList();
        }
    }
}
