using System.Text.Json;
using Microsoft.AspNetCore.Mvc;

using MesapInformationSystem;
using MesapInformationSystem.Types;
using System.Xml.Linq;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddSingleton(MesapInfoService.Instance);

var app = builder.Build();

app.MapGet("/users", async (MesapInfoService service,
                           [FromQuery(Name = "callback")] string callback) =>
{
    app.Logger.LogDebug($"Request received, callback is {callback}, getting user info...");
    List<User> users = await service.GetUserList();

    app.Logger.LogInformation($"User info retrieved, sending response for {users.Count} users.");
    return Results.Content($"{callback}({JsonSerializer.Serialize(users)})");
});
app.MapGet("/changes", async (MesapInfoService service,
                              [FromQuery(Name = "callback")] string callback,
                              [FromQuery(Name = "hours")] int hoursBack = 24,
                              [FromQuery(Name = "values")] bool values = true) =>
{
    app.Logger.LogDebug($"Request received, callback is {callback}, getting change info...");
    List<Change> changes = await service.GetLatestChanges(hoursBack, values);

    app.Logger.LogInformation($"Change info retrieved, sending response for {changes.Count} changes.");
    return Results.Content($"{callback}({JsonSerializer.Serialize(changes)})");
});

app.Run();

app.Logger.LogInformation("Shutting down...");
MesapInfoService.Instance.Shutdown();
