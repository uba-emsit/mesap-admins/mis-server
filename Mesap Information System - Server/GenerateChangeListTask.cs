﻿using System.Collections.Generic;
using System.Data.SqlClient;

using Mesap.Framework;
using Mesap.Framework.Entities;

using MesapInformationSystem.Types;

namespace MesapInformationSystem
{
    internal class GenerateChangeListTask 
    {
        private const int MAX_OBJECT_CHANGES_PER_DATABASE = 1000;
        private const int MAX_VALUE_CHANGES_PER_DATABASE = 10000;

        private static readonly Dictionary<string, string> MesapObjectTypes = new()
        {
            { "REPORT", "Report" },
            { "CALCULATION", "CalculationMethod" },
            { "TREE", "Tree" },
            { "DESCRIPTOR", "TreeObject" },
            { "SERIES", "TimeSeries" },
            { "VIEW", "TimeSeriesView" },
        };
        private static readonly Dictionary<string, string> UserNameLookup = [];

        internal static List<Change> Generate(SystemDatabase systemDB, int hoursBack, bool includeValues)
        {
            // 1. Prepare lookup table for future use
            if (UserNameLookup.Count == 0)
            {
                foreach (UserInfo user in systemDB.UserInfos)
                    UserNameLookup.Add(user.Id, user.Name);
            }

            // 2. Find databases to consider and search those one by one
            List<Change> changes = [];

            Dictionary<string, string> databasesToSearch = SelectDatabases(hoursBack);
            foreach (KeyValuePair<string, string> database in databasesToSearch)
            {
                string connectionString = $"{database.Value};User ID={Private.SQL_PROJECT_USERNAME};Password={Private.SQL_PROJECT_PASSWORD}";
                using (SqlConnection connection = new(connectionString))
                {
                    connection.Open();
                    changes.AddRange(CollectChanges(database.Key, connection, hoursBack, includeValues));
                }
            }                       

            return changes.OrderByDescending(change => change.ChangeDate).ToList();
        }

        /// <summary>
        /// Query the system database to find project databases that need to be search.
        /// We only search enabled databases that where accessed recently.
        /// </summary>
        /// <param name="hoursBack">Cut-off time point for change to be considered.</param>
        /// <returns>Dictionary of databases that should be searched for changes.
        /// Result format is key: database_id and value: connection_string.</returns>
        private static Dictionary<string, string> SelectDatabases(int hoursBack)
        {
            Dictionary<string, string> databases = [];

            string connectionString = @$"Data Source={Private.SQL_SYSTEM_DB_SERVER};Initial Catalog={Private.SQL_SYSTEM_DB_NAME};
                                         User ID={Private.SQL_SYSTEM_USERNAME};Password={Private.SQL_SYSTEM_PASSWORD}";
            using (SqlConnection connection = new(connectionString))
            {
                connection.Open();
                string sqlQuery = $"SELECT Id, ConnectionString FROM DbList WHERE Leaf = 1 and Disabled = 0 and AccessTime > @after";

                using (SqlCommand cmd = new(sqlQuery, connection))
                {
                    cmd.Parameters.AddWithValue("@after", DateTime.Now.AddHours(-hoursBack));
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                            databases.Add(reader.GetString(0), reader.GetString(1));
                    }
                }
            }

            return databases;
        }

        private static List<Change> CollectChanges(string databaseId, SqlConnection connection, int hoursBack, bool includeValues)
        {
            List<Change> changes = [];

            // 1. Find all changes to Mesap object (trees, views, time series etc.)
            foreach (KeyValuePair<string, string> entry in MesapObjectTypes)
                changes.AddRange(CollectObjectChanges(databaseId, connection, objectName: entry.Key, tableName: entry.Value, hoursBack));
            
            // 2. Add changes done to time series values
            if (includeValues)
                changes.AddRange(CollectValueChanges(databaseId, connection, hoursBack));

            return changes;
        }

        private static List<Change> CollectObjectChanges(string databaseId, SqlConnection connection, string objectName, string tableName, int hoursBack)
        {
            List<Change> changes = [];
            string sqlQuery = $"SELECT TOP (@limit) Name, Id, ChangeID, ChangeDate FROM {tableName} WHERE ChangeDate > @after ORDER BY ChangeDate DESC";

            using (SqlCommand cmd = new(sqlQuery, connection))
            {
                cmd.Parameters.AddWithValue("@limit", MAX_OBJECT_CHANGES_PER_DATABASE);
                cmd.Parameters.AddWithValue("@after", DateTime.Now.AddHours(-hoursBack));
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        changes.Add(new Change()
                        {
                            Database = databaseId,
                            Type = objectName,
                            Name = reader.GetString(0),
                            Id = reader.GetString(1),
                            User = UserNameLookup.TryGetValue(reader.GetString(2), out var value) ? value : reader.GetString(2),
                            ChangeDate = reader.GetDateTime(3)
                        });
                    }
                }
            }

            return changes;
        }

        private static List<Change> CollectValueChanges(string databaseId, SqlConnection connection, int hoursBack)
        {
            List<Change> changes = [];
            string sqlQuery =
                    @"SELECT TOP (@limit) TimeSeriesData.PeriodNr, TimeSeries.Id, TimeSeries.Name, TimeSeriesData.ChangeName, 
                    TimeSeriesData.ChangeDate FROM TimeSeriesData INNER JOIN TimeSeries ON TimeSeriesData.TsNr = TimeSeries.TsNr
                    WHERE TimeSeriesData.ChangeDate > @after ORDER BY TimeSeriesData.ChangeDate DESC";

            using (SqlCommand cmd = new(sqlQuery, connection))
            {
                cmd.Parameters.AddWithValue("@limit", MAX_VALUE_CHANGES_PER_DATABASE);
                cmd.Parameters.AddWithValue("@after", DateTime.Now.AddHours(-hoursBack));
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        changes.Add(new Change()
                        {
                            Database = databaseId,
                            Type = $"VALUE {reader.GetInt32(0) + 2000}",
                            Name = reader.GetString(2),
                            Id = reader.GetString(1),
                            User = UserNameLookup.TryGetValue(reader.GetString(3), out var value) ? value : reader.GetString(3),
                            ChangeDate = reader.GetDateTime(4)
                        });
                    }
                }
            }
            
            return changes;
        }
    }
}
