﻿using System.Text.Json.Serialization;

namespace MesapInformationSystem.Types
{
    public class Change
    {
        [JsonPropertyName("database")]
        public string Database { get; set; } = string.Empty;

        [JsonPropertyName("type")]
        public string Type { get; set; } = string.Empty;

        [JsonPropertyName("id")]
        public string Id { get; set; } = string.Empty;

        [JsonPropertyName("name")]
        public string Name { get; set; } = string.Empty;

        [JsonPropertyName("user")]
        public string User { get; set; } = string.Empty;

        [JsonPropertyName("datetime")]
        [JsonConverter(typeof(DateFormatConverter))]
        public DateTime ChangeDate { get; set; }
    }
}
