﻿using System.Text.Json.Serialization;

namespace MesapInformationSystem.Types
{
    public class User
    {
        [JsonPropertyName("name")]
        public string Name { get; set; } = string.Empty;

        [JsonPropertyName("loggedIn")]
        public string LoggedIn { get; set; } = string.Empty;

        [JsonPropertyName("databases")]
        public HashSet<string> Databases { get; set; } = [];

        [JsonPropertyName("lastSeenOnline")]
        [JsonConverter(typeof(DateFormatConverter))]
        public DateTime LastSeen { get; set; }
    }
}
